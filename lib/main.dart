import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';


void main() {
  runApp(new MaterialApp(
   home: MyHomePage(),
  ));
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      Navigator.of(context).pop();
      _image = image;
      cropImage(_image);
    });
  }

  Future openGallery() async{
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    cropImage(image);
    setState(() {
      Navigator.of(context).pop();
      //_image = image;
      
    });
  }
  void _showAlert(){
    AlertDialog dialog = new AlertDialog(
      actions: <Widget>[
        new FlatButton(onPressed: (){getImage();}, child: new Text("Camara")),
        new FlatButton(onPressed: (){openGallery();}, child: new Text("Galeria")),
        new FlatButton(onPressed: (){Navigator.of(context).pop();}, child: new Text("Cancelar")),
      ],
    );
    showDialog(context: context, child: dialog);
  }
  

  // Metodo para editar
  Future<Null> cropImage(File imageCropp) async {
      File croppedFile = await ImageCropper.cropImage(
        sourcePath: imageCropp.path,
        ratioX: 1.0,
        ratioY: 1.0,
        maxWidth: 512,
        maxHeight: 512,
      );

    setState(() {
      _image = croppedFile;  
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image Picker Example'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.mode_edit), onPressed: (){cropImage(_image);},)
        ],
      ),
      body: Center(
        child:
            _image == null
            ? Text('No image selected.')
            : Image.file(_image), 
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _showAlert,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
      
      
    );
  }
}